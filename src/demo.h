#ifndef DEMO_H
#define DEMO_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <json-c/json.h>

int demo_hello(json_object* input, json_object* output);

#endif /* demo.h */