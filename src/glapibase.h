/*
 * gllibbase.h
 *
 * This is a base file for gl lib development. Interfaces
 * defined here will be called automatically to register your lib
 * to gl-arch framework. Do not modify this file unless you got
 * the permission from the architect.
 *
 *  Created on: Apr 25, 2017
 *      Author: Xiaoyong Wei
 *
 *  modify by lancer @ 2018.03.30
 */

#ifndef GLLIBBASE_H_
#define GLLIBBASE_H_


#include <stdio.h>
#include <json-c/json.h>


typedef int (*api_call)(json_object*,json_object*);

typedef struct _api_info{
	/* the string for the function */	
	const char* Path;
	/* the allowed operations of "get" or "post" */
	const char* AllowedOperations;
	/* the handle of the target function */
	api_call TargetFunctionHandler;
}api_info_t;

#define map(path, opts, handler) {.Path=path,.AllowedOperations=opts,.TargetFunctionHandler=handler}


extern api_info_t* get_api_entity(int* pLen);


#endif /* GLLIBBASE_H_ */
