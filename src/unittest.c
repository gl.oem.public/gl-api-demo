#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "demo.h"

void test_demo_hello()
{
	json_object* input = json_object_new_object();
	json_object* output = json_object_new_object();
	
	demo_hello(input, output);
	printf("\n/demo/hello result:\n%s\n", json_object_to_json_string(output));
	
	json_object_put(input);
	json_object_put(output);
}

void test_demo_hostname_get()
{
	json_object* input = json_object_new_object();
	json_object* output = json_object_new_object();
	
	demo_hostname_get(input, output);
	printf("\n/demo/hostname_get result:\n%s\n", json_object_to_json_string(output));
	
	json_object_put(input);
	json_object_put(output);
}

void test_demo_hostname_set(char *value)
{
	json_object* input = json_object_new_object();
	json_object* output = json_object_new_object();

	json_object_object_add(input, "hostname", json_object_new_string("DEMO"));

	demo_hostname_set(input, output);
	printf("\n/demo/hostname_set result:\n%s\n", json_object_to_json_string(output));
	
	json_object_put(input);
	json_object_put(output);
}


int main(int argc , char *argv[])
{
	test_demo_hello();
	test_demo_hostname_get();
	if (argv[1] == NULL) {
		printf("\n/demo/hostname_set result:\nUsage: demotest <hostname>\n\n");
		return -1;
	}
	test_demo_hostname_set(argv[1]);
	
	return 0;
}
