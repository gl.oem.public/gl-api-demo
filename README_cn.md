
<!-- TOC -->

- [GL.iNet 路由器API开发指南](#glinet-路由器api开发指南)
    - [API封装](#api封装)
    - [API入口](#api入口)
    - [API文档](#api文档)
    - [API编写](#api编写)
    - [API编译](#api编译)
    - [API安装](#api安装)
    - [API调试](#api调试)

<!-- /TOC -->

# GL.iNet 路由器API开发指南

本篇文档将会介绍GL.iNet路由器API开发指南，你可以从中了解到如何编写及调试API程序。

## API封装

```
/* gllibbase.h */

typedef struct _api_info{
	/* the string for the function */	
	const char* Path;
	/* the allowed operations of "get" or "post" */
	const char* AllowedOperations;
	/* the handle of the target function */
	api_call TargetFunctionHandler;
} api_info_t;

#define map(path, opts, handler) { \
	.Path=path, \
	.AllowedOperations=opts, \
	.TargetFunctionHandler=handler \
}
```

你需要根据API的封装来定义一个API的数组变量。详情请参考测试demo。

## API入口

```
extern api_info_t* get_api_entity(int* pLen);
```

你需要实现该函数定义，将API入口暴露给API解析器。详情请参考测试demo。

## API文档

https://dev.gl-inet.com/router-api/2/，使用该API文档请将固件升级至3.025版本之上。

## API编写

请参考demo程序进行开发。可直接在demo程序的基础上进行添加和修改。

## API编译

程序编译可使用OpenWrt源码编译，也可以使用OpenWrt SDK编译，后者编译比较快。

- OpenWrt Source Code：https://github.com/gl-inet/openwrt

1. 下载OpenWrt源码，`git clone https://github.com/gl-inet/openwrt.git openwrt`

2. 根据 https://github.com/gl-inet/openwrt/blob/develop/README.md 文档进行环境搭建

3. 将demo程序拷贝至 openwrt/package 目录下，执行命令 `make menuconfig` ，勾选gl-inet目录下的gl-api-demo

4. 执行编译命令 `make package/gl-api-demo/compile`，ipk程序会在bin/目录下生成

- OpenWrt SDK：https://github.com/gl-inet/sdk

1. 下载OpenWrt SDK，`git clone https://github.com/gl-inet/sdk.git sdk`

2. 根据 https://github.com/gl-inet/sdk/blob/master/README.md 文档进行环境搭建

3. 将demo程序拷贝至 sdk/\<target>/package 目录下，执行命令 `make menuconfig` ，勾选gl-inet目录下的gl-api-demo目录下

4. 执行编译命令 `make package/gl-api-demo/compile`，ipk程序会在bin/目录下生成

## API安装

使用WinSCP工具将ipk上传到路由器/tmp/目录进行安装，WinSCP下载地址 https://winscp.net/eng/download.php，安装与卸载命令如下：

- 安装：opkg install gl-api-demo_3.0.0-1_mips_24kc.ipk
- 卸载：opkg remove gl-api-demo

## API调试

- curl

1. 获取token

```
# curl -X POST 192.168.8.1/cgi-bin/api/router/login -d "pwd=goodlife1"
{"code":0,"token":"ad959856a9894adcbcb07ade1eda63bf"}
```

2. /demo/hello

```
# curl -X GET -H "Authorization: ad959856a9894adcbcb07ade1eda63bf" 192.168.8.1/cgi-bin/api/demo/hello
{"code":0,"msg":"Hello GL.iNet API"}
```

3. /demo/hostname_get

```
# curl -X GET -H "Authorization: ad959856a9894adcbcb07ade1eda63bf" 192.168.8.1/cgi-bin/api/demo/hostname_get
{"code":0,"hostname":"GL-USB150"}
```

4. /demo/hostname_set

```
# curl -X POST -H "Authorization: ad959856a9894adcbcb07ade1eda63bf" 192.168.8.1/cgi-bin/api/demo/hostname_set -d "hostname=GL-S1300"
{"code":0}
```

- Postman

1. 获取token

> Headers parameter

Content-Type：application/x-www-form-urlencoded

> Body parameter

编码格式：x-www-form-urlencoded

KEY：pwd，VALUE：goodlife1

> response

```
{
    "code": 0,
    "token": "89ae4e1f76064ba38fa844aaaaa70b3a"
}
```

2. /demo/hello

> Headers parameter

Content-Type：application/x-www-form-urlencoded

Authorization：89ae4e1f76064ba38fa844aaaaa70b3a

> Body parameter

编码格式：x-www-form-urlencoded

> response

```
{
    "code": 0,
    "msg": "Hello GL.iNet API"
}
```

3. /demo/hostname_get

> Headers parameter

Content-Type：application/x-www-form-urlencoded

Authorization：89ae4e1f76064ba38fa844aaaaa70b3a

> Body parameter

编码格式：x-www-form-urlencoded

> response

```
{
    "code": 0,
    "hostname": "GL-USB150"
}
```

4. /demo/hostname_set

> Headers parameter

Content-Type：application/x-www-form-urlencoded

Authorization：89ae4e1f76064ba38fa844aaaaa70b3a

> Body parameter

编码格式：x-www-form-urlencoded

KEY：hostname，VALUE：test

> response

```
{
    "code": 0
}
```

